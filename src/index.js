"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var lotion_state_machine_1 = require("lotion-state-machine");
var path_1 = require("path");
var os_1 = require("os");
var abci_server_1 = require("./abci-server");
var tendermint_1 = require("./tendermint");
var discovery_1 = require("./discovery");
var crypto_1 = require("crypto");
var fs = require("fs-extra");
var getPort = require("get-port");
var DJSON = require("deterministic-json");
var level = require("level");
var merk = require("merk");
var LotionApp = /** @class */ (function () {
    function LotionApp(config) {
        this.config = config;
        this.discovery = true;
        this.lotionHome = path_1.join(os_1.homedir(), '.lotion', 'networks');
        this.application = lotion_state_machine_1["default"](config);
        this.logTendermint = config.logTendermint;
        this.initialState = config.initialState;
        this.keyPath = config.keyPath;
        this.genesisPath = config.genesisPath;
        this.peers = config.peers;
        this.discovery = config.discovery == null ? true : config.discovery;
        this.setHome();
        Object.assign(this, this.application);
    }
    LotionApp.prototype.assignPorts = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _a, _b, _c, _d, _e;
            return __generator(this, function (_f) {
                switch (_f.label) {
                    case 0:
                        _a = this;
                        _b = {};
                        _c = this.config.abciPort;
                        if (_c) return [3 /*break*/, 2];
                        return [4 /*yield*/, getPort()];
                    case 1:
                        _c = (_f.sent());
                        _f.label = 2;
                    case 2:
                        _b.abci = _c;
                        _d = this.config.p2pPort;
                        if (_d) return [3 /*break*/, 4];
                        return [4 /*yield*/, getPort()];
                    case 3:
                        _d = (_f.sent());
                        _f.label = 4;
                    case 4:
                        _b.p2p = _d;
                        _e = this.config.rpcPort;
                        if (_e) return [3 /*break*/, 6];
                        return [4 /*yield*/, getPort()];
                    case 5:
                        _e = (_f.sent());
                        _f.label = 6;
                    case 6:
                        _a.ports = (_b.rpc = _e,
                            _b);
                        return [2 /*return*/];
                }
            });
        });
    };
    LotionApp.prototype.setGCI = function () {
        this.GCI = crypto_1.createHash('sha256')
            .update(this.genesis)
            .digest('hex');
    };
    LotionApp.prototype.getAppInfo = function () {
        return {
            ports: this.ports,
            GCI: this.GCI,
            genesisPath: this.genesisPath,
            home: this.home
        };
    };
    LotionApp.prototype.setGenesis = function () {
        if (!this.genesisPath) {
            this.genesisPath = path_1.join(this.home, 'config', 'genesis.json');
        }
        var genesisJSON = fs.readFileSync(this.genesisPath, 'utf8');
        var parsedGenesis = JSON.parse(genesisJSON);
        this.genesis = DJSON.stringify(parsedGenesis);
    };
    LotionApp.prototype.setHome = function () {
        /**
         * if genesis and key paths are provided,
         * home path is hash(genesisPath + keyPath)
         *
         * otherwise a random id is generated.
         */
        if (this.config.genesisPath && this.config.keyPath) {
            this.home = path_1.join(this.lotionHome, crypto_1.createHash('sha256')
                .update(fs.readFileSync(this.config.genesisPath))
                .update(fs.readFileSync(this.config.keyPath))
                .digest('hex'));
        }
        else if (this.config.genesisPath && !this.config.keyPath) {
            this.home = path_1.join(this.lotionHome, crypto_1.createHash('sha256')
                .update(fs.readFileSync(this.config.genesisPath))
                .digest('hex'));
        }
        else {
            this.home = path_1.join(this.lotionHome, crypto_1.randomBytes(16).toString('hex'));
        }
    };
    LotionApp.prototype.start = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _a, _b, appInfo;
            return __generator(this, function (_c) {
                switch (_c.label) {
                    case 0: return [4 /*yield*/, this.assignPorts()];
                    case 1:
                        _c.sent();
                        return [4 /*yield*/, fs.mkdirp(this.home)
                            // create or load state db
                        ];
                    case 2:
                        _c.sent();
                        // create or load state db
                        this.db = level(path_1.join(this.home, 'state.db'));
                        _a = this;
                        return [4 /*yield*/, merk(this.db)
                            // start state machine
                        ];
                    case 3:
                        _a.state = _c.sent();
                        // start state machine
                        this.stateMachine = this.application.compile(this.state);
                        this.abciServer = abci_server_1["default"](this.state, this.stateMachine, this.initialState, this.home);
                        this.abciServer.listen(this.ports.abci);
                        // start tendermint process
                        _b = this;
                        return [4 /*yield*/, tendermint_1["default"]({
                                ports: this.ports,
                                home: this.home,
                                logTendermint: this.logTendermint,
                                keyPath: this.keyPath,
                                genesisPath: this.genesisPath,
                                peers: this.peers
                            })];
                    case 4:
                        // start tendermint process
                        _b.tendermintProcess = _c.sent();
                        this.setGenesis();
                        this.setGCI();
                        // start discovery server
                        this.discoveryServer = discovery_1["default"]({
                            GCI: this.GCI,
                            rpcPort: this.ports.rpc
                        });
                        appInfo = this.getAppInfo();
                        return [2 /*return*/, appInfo];
                }
            });
        });
    };
    return LotionApp;
}());
var Lotion = function (config) {
    return new LotionApp(config);
};
Lotion.connect = require('lotion-connect');
module.exports = Lotion;
